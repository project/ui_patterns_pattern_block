<?php

namespace Drupal\ui_patterns_pattern_block\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\ui_patterns\UiPatterns;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PatternBlock extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Constructs new NodeBlock.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $definitions = UiPatterns::getPatternDefinitions();
    foreach ($definitions as $id => $definition) {
      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['admin_label'] = $definition->getLabel();
      $this->derivatives[$id]['id'] = $id;
    }
    return $this->derivatives;
  }

}

<?php
namespace Drupal\ui_patterns_pattern_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ui_patterns\UiPatterns;
use Drupal\ui_patterns\UiPatternsManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ui_patterns_settings\Element\PatternSettings;
use Drupal\ui_patterns_settings\Form\SettingsFormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
* Provides a generic Menu block.
*
* @Block(
*   id = "pattern_block",
*   admin_label = @Translation("UI Patterns Block"),
*   category = @Translation("UI Patterns"),
*   deriver = "Drupal\ui_patterns_pattern_block\Plugin\Derivative\PatternBlock"
* )
*/
class PatternBlock extends BlockBase implements ContainerFactoryPluginInterface {


  /**
   * UI Patterns manager.
   *
   * @var \Drupal\ui_patterns\UiPatternsManager
   */
  protected $patternsManager;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\ui_patterns\UiPatternsManager $patterns_manager
   *   UI Patterns manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UiPatternsManager $patterns_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $patterns_manager);
    $this->configuration = $configuration;
    $this->patternsManager = $patterns_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.ui_patterns')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['id' => NULL];
  }

  protected function getPatternId() {
    return explode(':', $this->getPluginId())[1];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return [
      '#type' => 'pattern_preview',
      '#context' => ['type' => 'pattern_block'],
      '#id' => $this->getPatternId(),
      '#settings'=> $config['pattern']['settings'],
      '#variant' => $config['pattern']['variant'] ?? NULL
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $definition = UiPatterns::getPatternDefinition($this->getPatternId());

    $form['variant'] = [
      '#type' => 'select',
      '#empty_value' => '_none',
      '#title' => $this->t('Variant'),
      '#options' => $definition->getVariantsAsOptions(),
      '#default_value' => isset($config['variant']) ? $config['variant'] : NULL,
      '#required' => TRUE,
      '#attributes' => ['id' => 'patterns-select'],
      '#access' => count($definition->getVariantsAsOptions()) > 0
    ];
    SettingsFormBuilder::layoutForm($form, $definition, $configuration);
    $form['settings']['#type'] = 'container';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['pattern']['settings'] = $values['settings'];
    if (isset($values['variant'])) {
      $this->configuration['pattern']['variant'] = $values['variant'];
    }
  }

}

# UI Patterns Pattern Block

Exports each pattern as a block and provide the UI Patterns Settings Form .


## Installation / configuration:
Simply install and enable the module. Don't forget to enable ui_patterns_layout (If you don't use another layout discovery module.).

No more configuration is needed and every pattern can be used as a layout in the layout builder.

The development is still in very early stage.




